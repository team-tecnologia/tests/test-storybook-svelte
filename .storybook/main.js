const path = require('path');
const sveltePreprocess = require('svelte-preprocess');

module.exports = {
  svelteOptions: {
    preprocess: sveltePreprocess(),
  },
  "stories": [
    "../src/**/*.stories.mdx",
    "../src/**/*.stories.@(js|jsx|ts|tsx|svelte)"
  ],
  "addons": [
    '@storybook/addon-a11y',
    '@storybook/preset-scss',
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-svelte-csf",
    'creevey'
  ],
  webpackFinal: (config) => {
    // fix for storybook webpack5 + svelte
    config.resolve.alias.svelte = path.resolve('node_modules', 'svelte')

    
    return config;
  },
  core: {
    builder: 'webpack5',
  },
}