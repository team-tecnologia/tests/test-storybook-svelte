import PgRequest from "./PgRequest.svelte"
import userData from '../../../__fixtures__/userData'

import { action } from '@storybook/addon-actions';

export default {
    title: 'Pages/Request',
    component: PgRequest,
}

const Template = (_args) => {
    const ret = ({ ...props }) => ({
        Component: PgRequest,
        props,
        on: {
            getRandomUser: action('getRandomUser')
        }
    })
    ret.args = _args
    return ret
}

export const Default = Template({
    userDataPromise: Promise.resolve(userData)
})

export const Loading = Template({
    userDataPromise: new Promise(() => {})
})

export const Error = Template({
    userDataPromise: Promise.reject("TypeError: Failed to fetch")
})