import Button from "./Button.svelte";
import ButtonWrapperForTestSlot from './ButtonWrapperForTestSlot.svelte';
import ButtonWrapperCount from './ButtonWrapperCount.svelte'

import { action } from '@storybook/addon-actions';

export default {
    title: 'Components/Button/ts-version',
    component: Button,
    argTypes: {
        onClick: { action: 'onClick' },
        primary: { control: 'boolean' },
        labelSlot: { control: 'text' },
        size: {
        control: {
            type: 'select',
            options: ['small', 'medium', 'large']
        },
        },
        backgroundColor: { control: 'color' }
    }
}

const Template = (_args) => {
    const ret = ({ ...props }) => ({
        // note that export default use Button componente, because this file describe button stories
        // this ButtonWrapperForTestSlot is only for wrap slots
        Component: ButtonWrapperForTestSlot,
        props,
        on: {
            click: action('onClick'),
        }
    })
    ret.args = _args
    return ret
}

export const Primary = Template({
    primary: true,
    labelSlot: 'Button',
})

export const Secondary = Template({
    labelSlot: "Button",
})

export const Large = Template({
    size: "large",
    labelSlot: "Button",
})

export const Small = Template({
    size: "small",
    labelSlot: "Button",
})

export const Inaccessible  = Template({
    primary: true,
    backgroundColor: "white",
    labelSlot: "Button",
})

export const TestWithCreevey :any = () => ({
    // note that export default use Button componente, because this file describe button stories
    // this ButtonWrapperCount is only for wrap the onclick to teste interactivity
    Component: ButtonWrapperCount,
    props: {
    },
    on: {
        click: action('onClick'),
    }
})

/**
 * Create a test for creevey that will capture 2 different screen shots after click on the screen
 */
 TestWithCreevey.parameters = {
    creevey: {
        delay: 1000,
        tests: {
            async click() {
                const buttonEl = await this.browser.findElement({ css: 'button' });
                
                // must increase the count
                await this.browser.actions().click(buttonEl).perform()
                const p1 = await this.takeScreenshot()
                
                // must increase the count again and take another print
                await this.browser.actions().click(buttonEl).perform()
                const p2 = await this.takeScreenshot()

                await this.expect({ p1, p2 }).to.matchImages()
            },
        },
    }
}
